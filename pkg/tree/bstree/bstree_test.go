package bstree

import (
	"testing"
)

// Following tree is created
//             8
//           /   \
//         /      \
//        4       10
//      /   \    /  \
//     2     6  9   11
//    / \   / \
//   /   \ 5   7
//  1    3
func testTree() *BinarySearchTree {
	bst := New()
	bst.Insert(8, "8")
	bst.Insert(4, "4")
	bst.Insert(10, "10")
	bst.Insert(2, "2")
	bst.Insert(6, "6")
	bst.Insert(1, "1")
	bst.Insert(3, "3")
	bst.Insert(5, "5")
	bst.Insert(7, "7")
	bst.Insert(9, "9")
	bst.Insert(11, "11")

	return bst
}

func TestBinarySearchTree_Insert(t *testing.T) {
	tree := testTree()

	if tree.root == nil {
		t.Fatalf("Expected root to be not nil, got nil")
	}

	if tree.root.left == nil {
		t.Fatalf("Expected leaf with key 4 to be not nil, got nil")
	}

	if tree.root.right == nil {
		t.Fatalf("Expected leaf with key 10 to be not nil, got nil")
	}

	if tree.root.left.left == nil {
		t.Fatalf("Expected leaf with key 2 to be not nil, got nil")
	}

	if tree.root.left.right == nil {
		t.Fatalf("Expected leaf with key 6 to be not nil, got nil")
	}

	if tree.root.left.left.left == nil {
		t.Fatalf("Expected leaf with key 1 to be not nil, got nil")
	}

	if tree.root.left.left.right == nil {
		t.Fatalf("Expected leaf with key 3 to be not nil, got nil")
	}

	if tree.root.left.right.left == nil {
		t.Fatalf("Expected leaf with key 5 to be not nil, got nil")
	}

	if tree.root.left.right.right == nil {
		t.Fatalf("Expected leaf with key 7 to be not nil, got nil")
	}

	if tree.root.right.left == nil {
		t.Fatalf("Expected leaf with key 9 to be not nil, got nil")
	}

	if tree.root.right.right == nil {
		t.Fatalf("Expected leaf with key 11 to be not nil, got nil")
	}

	// Updating leaf 10 with another value, should keep its children
	tree.Insert(10, "new value")

	if tree.root.right.value != "new value" {
		t.Fatalf("Expected leaf with key 10 to has value %s, got %s", "new value", tree.root.right.value)
	}

	if tree.root.right.left == nil {
		t.Fatalf("Expected leaf with key 9 to be not nil, got nil")
	}

	if tree.root.right.right == nil {
		t.Fatalf("Expected leaf with key 11 to be not nil, got nil")
	}
}

func TestBinarySearchTree_Min(t *testing.T) {
	tree := testTree()

	min := tree.Min()

	if min != "1" {
		t.Fatalf("Expected tree min leaf's value to be %s, got %s", "1", min)
	}
}

func TestBinarySearchTree_Max(t *testing.T) {
	tree := testTree()

	max := tree.Max()

	if max != "11" {
		t.Fatalf("Expected tree max leaf's value to be %s, got %s", "11", max)
	}

}

func TestBinarySearchTree_Get(t *testing.T) {
	tree := testTree()

	type args struct {
		key int
	}
	tests := []struct {
		name  string
		bst   *BinarySearchTree
		args  args
		value interface{}
		ok    bool
	}{
		{
			"Key exists",
			tree,
			args{1},
			"1",
			true,
		},
		{
			"Key does not exist",
			tree,
			args{100000},
			nil,
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			value, ok := tt.bst.Get(tt.args.key)
			if value != tt.value {
				t.Errorf("BinarySearchTree.Get() got value = %v, want %v", value, tt.value)
			}

			if ok != tt.ok {
				t.Errorf("BinarySearchTree.Get() got ok = %v, want %v", ok, tt.ok)
			}
		})
	}
}

func TestBinarySearchTree_Remove(t *testing.T) {
	tree := testTree()

	// Deleting leftmost leaf
	tree.Remove(1)

	if val, ok := tree.Get(1); val != nil || ok {
		t.Fatalf("Expected leaf with key 1 to not exist in tree, got val: %s, ok: %t", val, ok)
	}

	// Deleting rightmost leaf
	tree.Remove(11)

	if val, ok := tree.Get(11); val != nil || ok {
		t.Fatalf("Expected leaf with key 11 to not exist in tree, got val: %s, ok: %t", val, ok)
	}

	// Deleting leaf with only left node
	tree.Remove(10)

	if val, ok := tree.Get(10); val != nil || ok {
		t.Fatalf("Expected leaf 10 to not exist in tree, got val: %s, ok: %t", val, ok)
	}

	if tree.root.right.key != 9 {
		t.Fatalf("Expected root's right leaf to have key %d, got %d", 9, tree.root.right.key)
	}

	// Deleting leaf with only right leaf with right leaf
	tree.Remove(5)

	if val, ok := tree.Get(5); val != nil || ok {
		t.Fatalf("Expected leaf 5 to not exist in tree, got val: %s, ok: %t", val, ok)
	}

	// Leaf 6 should take place of leaf 4
	tree.Remove(4)

	if val, ok := tree.Get(4); val != nil || ok {
		t.Fatalf("Expected leaf 4 to not exist in tree, got val: %s, ok: %t", val, ok)
	}

	if tree.root.left.key != 6 {
		t.Fatalf("Expected root's left leaf to have key %d, got %d", 6, tree.root.left.key)
	}

	// Re-create tree to test one of possible cases (deleted leaf has right child leaf, which has left child leaf)
	tree = testTree()

	tree.Remove(1)
	tree.Remove(3)
	tree.Remove(7)

	// Leaf 5 should take place of leaf 4
	tree.Remove(4)

	if tree.root.left.key != 5 {
		t.Fatalf("Expected root's left leaf to have key %d, got %d", 5, tree.root.left.key)
	}
}
