package bstree

// Node ...
type Node struct {
	key   int
	value interface{}
	left  *Node
	right *Node
}

// BinarySearchTree ...
type BinarySearchTree struct {
	root *Node
}

// New ...
func New() *BinarySearchTree {
	return &BinarySearchTree{}
}

// Empty ...
func (bst *BinarySearchTree) Empty() bool {
	return bst.root != nil
}

// Size ...
func (bst *BinarySearchTree) Size() int {
	size := 0

	bst.PostOrderTraverse(func(value interface{}) {
		size++
	})

	return size
}

// Clear ...
func (bst *BinarySearchTree) Clear() {
	bst.root = nil
}

// Values ...
func (bst *BinarySearchTree) Values() []interface{} {
	out := make([]interface{}, 0)

	bst.InOrderTraverse(func(value interface{}) {
		out = append(out, value)
	})

	return out
}

// Insert ...
func (bst *BinarySearchTree) Insert(key int, value interface{}) {
	newNode := &Node{key, value, nil, nil}

	if bst.root == nil {
		bst.root = newNode
	} else {
		insertNode(bst.root, newNode)
	}
}

func insertNode(node, newNode *Node) {
	if newNode.key < node.key {
		if node.left == nil {
			node.left = newNode
		} else if node.left.key == newNode.key {
			node.left.value = newNode.value
		} else {
			insertNode(node.left, newNode)
		}
	} else {
		if node.right == nil {
			node.right = newNode
		} else if node.right.key == newNode.key {
			node.right.value = newNode.value
		} else {
			insertNode(node.right, newNode)
		}
	}
}

// InOrderTraverse ...
func (bst *BinarySearchTree) InOrderTraverse(f func(value interface{})) {
	var traverse func(*Node, func(interface{}))

	traverse = func(n *Node, f func(v interface{})) {
		if n != nil {
			traverse(n.left, f)
			f(n.value)
			traverse(n.right, f)
		}
	}

	traverse(bst.root, f)
}

// PreOrderTraverse ...
func (bst *BinarySearchTree) PreOrderTraverse(f func(value interface{})) {
	var traverse func(*Node, func(interface{}))

	traverse = func(n *Node, f func(v interface{})) {
		if n != nil {
			f(n.value)
			traverse(n.left, f)
			traverse(n.right, f)
		}
	}

	traverse(bst.root, f)
}

// PostOrderTraverse ...
func (bst *BinarySearchTree) PostOrderTraverse(f func(value interface{})) {
	var traverse func(*Node, func(interface{}))

	traverse = func(n *Node, f func(v interface{})) {
		if n != nil {
			traverse(n.left, f)
			traverse(n.right, f)
			f(n.value)
		}
	}

	traverse(bst.root, f)
}

// Min ...
func (bst *BinarySearchTree) Min() interface{} {
	n := bst.root

	if n == nil {
		return nil
	}

	for {
		if n.left == nil {
			return n.value
		}

		n = n.left
	}
}

// Max ...
func (bst *BinarySearchTree) Max() interface{} {
	n := bst.root

	if n == nil {
		return nil
	}

	for {
		if n.right == nil {
			return n.value
		}

		n = n.right
	}
}

// Get ...
func (bst *BinarySearchTree) Get(key int) (interface{}, bool) {
	var search func(*Node, int) (interface{}, bool)

	search = func(n *Node, key int) (interface{}, bool) {
		if n == nil {
			return nil, false
		}

		if key < n.key {
			return search(n.left, key)
		}

		if key > n.key {
			return search(n.right, key)
		}

		return n.value, true
	}

	return search(bst.root, key)
}

// Remove ...
func (bst *BinarySearchTree) Remove(key int) {
	remove(bst.root, key)
}

func remove(n *Node, key int) *Node {
	if n == nil {
		return nil
	}

	if key < n.key {
		n.left = remove(n.left, key)
		return n
	}

	if key > n.key {
		n.right = remove(n.right, key)
		return n
	}

	if n.left == nil && n.right == nil {
		n = nil
		return nil
	}

	if n.left == nil {
		n = n.right
		return n
	}

	if n.right == nil {
		n = n.left
		return n
	}

	leftmostrightside := n.right

	for {
		if leftmostrightside != nil && leftmostrightside.left != nil {
			leftmostrightside = leftmostrightside.left
		} else {
			break
		}
	}

	n.key, n.value = leftmostrightside.key, leftmostrightside.value
	n.right = remove(n.right, n.key)

	return n
}
