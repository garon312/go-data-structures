package gomap

type HashMap struct {
	container map[interface{}]interface{}
}

func New() *HashMap {
	return &HashMap{container: make(map[interface{}]interface{})}
}

func (m *HashMap) Empty() bool {
	return len(m.container) == 0
}

func (m *HashMap) Size() int {
	return len(m.container)
}

func (m *HashMap) Clear() {
	m.container = make(map[interface{}]interface{})
}

func (m *HashMap) Values() []interface{} {
	values := make([]interface{}, m.Size())
	count := 0

	for _, value := range m.container {
		values[count] = value
		count++
	}

	return values
}

func (m *HashMap) Put(key, value interface{}) {
	m.container[key] = value
}

func (m *HashMap) Get(key interface{}) (val interface{}, ok bool) {
	val, ok = m.container[key]
	return
}

func (m *HashMap) Remove(key interface{}) {
	delete(m.container, key)
}

func (m *HashMap) Keys() []interface{} {
	keys := make([]interface{}, m.Size())
	count := 0

	for key := range m.container {
		keys[count] = key
		count++
	}

	return keys
}


