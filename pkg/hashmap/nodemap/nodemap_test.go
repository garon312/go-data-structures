package nodemap

import (
	"testing"
)

func TestHashMap_Put(t *testing.T) {
	input := map[string]int{
		"one": 1,
		"two": 2,
	}

	hashMap := New()

	for key, value := range input {
		hashMap.Put(key, value)

		if val, ok := hashMap.Get(key); val != value || !ok {
			t.Errorf("Expected value %v by key %v, got %v", value, key, val)
		}
	}

	if hashMap.Size() != len(input) {
		t.Errorf("Expected map size %v, got %v", len(input), hashMap.Size())
	}
}

func TestHashMap_Get(t *testing.T) {
	input := map[string]int{
		"test": 1,
		"case": 2,
	}

	hashMap := New()

	for key, value := range input {
		hashMap.Put(key, value)

		if val, ok := hashMap.Get(key); val != value || !ok {
			t.Errorf("Expected value %v by key %v, got %v", value, key, val)
		}
	}

	if _, ok := hashMap.Get("non existing key"); ok {
		t.Errorf("Expected non existing key, got ok")
	}
}

func TestHashMap_Remove(t *testing.T) {
	hashMap := New()

	input := map[string]int{
		"test": 1,
		"case": 2,
	}

	for key, value := range input {
		hashMap.Put(key, value)
		hashMap.Remove(key)

		if _, ok := hashMap.Get(key); ok {
			t.Errorf("Expected non existing key, got ok")
		}
	}

	if hashMap.Size() != 0 {
		t.Errorf("Expected hashmap to be empty, got %v", hashMap.Size())
	}
}

func TestHashMap_Keys(t *testing.T) {
	hashMap := New()

	input := map[interface{}]int{
		"test": 1,
		"case": 2,
	}

	for key, value := range input {
		hashMap.Put(key, value)
	}

	for _, v := range hashMap.Keys() {
		if key, ok := input[v]; !ok {
			t.Errorf("Expected input key %v to exist in input map, got none", key)
		}
	}
}

func TestHashMap_Values(t *testing.T) {
	hashMap := New()

	input := map[interface{}]interface{}{
		"test": 1,
		"case": 1,
		"multiple": 2,
		"values": 2,
	}

	valuesCount := make(map[interface{}]int)

	for _, v := range input {
		valuesCount[v]++
	}

	for key, value := range input {
		hashMap.Put(key, value)
	}

	hashMapValuesCount := make(map[interface{}]int)

	for _, v := range hashMap.Values() {
		hashMapValuesCount[v]++
	}

	for inputVal, inputValCount := range valuesCount {
		if hashValCount, ok := hashMapValuesCount[inputVal]; !ok || inputValCount != hashValCount {
			t.Errorf("Expected value %v to appear in hashmap %v times, got %v", inputVal, inputValCount, hashValCount)
		}
	}
}
