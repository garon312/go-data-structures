package nodemap

import (
	"fmt"
	"hash/fnv"
)

const (
	defaultCapacity = 1 << 10
)

type node struct {
	key  interface{}
	value  interface{}
	next *node
}

type HashMap struct {
	capacity uint64
	size     uint64
	table    []*node
}

func New() *HashMap {
	return &HashMap{
		capacity: defaultCapacity,
		table:    make([]*node, defaultCapacity),
	}
}

func (m *HashMap) Empty() bool {
	return m.size == 0
}

func (m *HashMap) Size() int {
	return int(m.size)
}

func (m *HashMap) Clear() {
	m.table = make([]*node, defaultCapacity)
	m.size = 0
	m.capacity = defaultCapacity
}

func (m *HashMap) Values() []interface{} {
	values := make([]interface{}, 0)
	for _, node := range m.table {
		if node == nil {
			continue
		}

		values = append(values, node.value)
	}

	return values
}

func (m *HashMap) Put(key, value interface{}) {
	if m.capacity == 0 {
		m.capacity = defaultCapacity
		m.table = make([]*node, defaultCapacity)
	}

	hash := m.hash(key)

	node := m.getByHash(hash)

	if node == nil {
		m.table[hash] = newNode(key, value)
	} else if node.key == key {
		node.value = value
	} else {
		m.resize()
		m.Put(key, value)
	}

	m.size++
}

func (m *HashMap) Get(key interface{}) (val interface{}, ok bool) {
	node := m.getByHash(m.hash(key))

	if node == nil {
		return nil, false
	}

	return node.value, true
}

func (m *HashMap) Remove(key interface{}) {
	m.table[m.hash(key)] = nil
	m.size--
}

func (m *HashMap) Keys() []interface{} {
	keys := make([]interface{}, 0)
	for _, node := range m.table {
		if node == nil {
			continue
		}

		keys = append(keys, node.key)
	}

	return keys
}

func newNode(key interface{}, value interface{}) *node {
	return &node{
		key: key,
		value: value,
	}
}

func (m *HashMap) getByHash(hash uint64) *node {
	return m.table[hash]
}

func (m *HashMap) hash(key interface{}) uint64 {
	h := fnv.New64a()
	_, _ = h.Write([]byte(fmt.Sprintf("%v", key)))

	hashValue := h.Sum64()

	return (m.capacity - 1) & (hashValue ^ (hashValue >> 16))
}

func (m *HashMap) resize() {
	m.capacity <<= 1
	temp := m.table

	m.table = make([]*node, m.capacity)

	for i := 0; i < len(temp); i++ {
		node := temp[i]

		if node == nil {
			continue
		}

		m.table[m.hash(node.key)] = node
	}
}
