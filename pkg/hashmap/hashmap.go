package hashmap

import "go-data-structures/pkg/container"

type HashMap interface {
	container.Container

	Put(key, value interface{})
	Get(key interface{}) (val interface{}, ok bool)
	Remove(key interface{})
	Keys() []interface{}
}