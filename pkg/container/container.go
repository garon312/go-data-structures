package container

// Container interface provides set of basic container methods
type Container interface {
	Empty() bool
	Size() int
	Clear()
	Values() []interface{}
}
