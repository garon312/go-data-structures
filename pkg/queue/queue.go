package queue

import "go-data-structures/pkg/container"

type Queue interface {
	Queue(value interface{})
	Dequeue() (interface{}, bool)
	Peek() (interface{}, bool)

	container.Container
}
