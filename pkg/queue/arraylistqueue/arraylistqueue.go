package arraylistqueue

import "go-data-structures/pkg/list/arraylist"

type Queue struct {
	list *arraylist.List
}

func New() *Queue {
	return &Queue{
		arraylist.New(),
	}
}

func (q *Queue) Queue(value interface{}) {
	q.list.Add(value)
}

func (q *Queue) Dequeue() (interface{}, bool) {
	value, ok := q.list.Get(0)
	q.list.Remove(0)
	return value, ok
}

func (q *Queue) Peek() (interface{}, bool) {
	return q.list.Get(0)
}

func (q *Queue) Empty() bool {
	return q.list.Size() == 0
}

func (q *Queue) Size() int {
	return q.list.Size()
}

func (q *Queue) Clear() {
	q.list.Clear()
}

func (q *Queue) Values() []interface{} {
	return q.list.Values()
}
