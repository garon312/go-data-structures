package arraylistqueue

import (
	"fmt"
	"testing"
)

func TestQueue_Queue(t *testing.T) {
	input := []int{1, 2, 3, 4}
	queue := New()

	for _, value := range input {
		queue.Queue(value)
	}

	if val, ok := queue.Peek(); val != input[0] || !ok {
		t.Errorf("Expected %v, got %v", input[0], val)
	}

	if queue.Size() != len(input) {
		t.Errorf("Expected %v, got %v", queue.Size(), len(input))
	}
}

func TestQueue_Dequeue(t *testing.T) {
	testCases := []struct {
		input        []interface{}
		expectedSize int
		expectedVal  interface{}
		expectedOk   bool
	}{
		{
			input:        []interface{}{1, 2, 3},
			expectedSize: 2,
			expectedVal:  1,
			expectedOk:   true,
		},
		{
			input:        []interface{}{1},
			expectedSize: 0,
			expectedVal:  1,
			expectedOk:   true,
		},
		{
			input:        []interface{}{},
			expectedSize: 0,
			expectedVal:  nil,
			expectedOk:   false,
		},
	}

	for _, testCase := range testCases {
		queue := New()

		for _, val := range testCase.input {
			queue.Queue(val)
		}

		value, ok := queue.Dequeue()

		if ok != testCase.expectedOk {
			t.Errorf("Expected %v, got %v", testCase.expectedOk, ok)
		}

		if value != testCase.expectedVal {
			t.Errorf("Expected %v, got %v", testCase.expectedVal, value)
		}

		if queue.Size() != testCase.expectedSize {
			t.Errorf("Expected %v, got %v", testCase.expectedSize, queue.Size())
		}
	}
}

func TestQueue_Values(t *testing.T) {
	input := []int{1, 2, 3}
	queue := New()

	for _, value := range input {
		queue.Queue(value)
	}

	if fmt.Sprint(input) != fmt.Sprint(queue.Values()) {
		t.Errorf("Expected %v, got %v", fmt.Sprint(input), fmt.Sprint(queue.Values()))
	}
}

func TestQueue_Peek(t *testing.T) {
	input := []int{1, 2, 3}
	queue := New()

	for _, value := range input {
		queue.Queue(value)
	}

	value, ok := queue.Peek()

	if !ok {
		t.Errorf("Expected %v, got %v", true, ok)
	}

	if value != input[0] {
		t.Errorf("Expected %v, got %v", input[0], value)
	}

	if queue.Size() != len(input) {
		t.Errorf("Expected %v, got %v", len(input), queue.Size())
	}
}
