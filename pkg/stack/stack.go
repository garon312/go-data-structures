package stack

import "go-data-structures/pkg/container"

type Stack interface {
	Pop() (interface{}, bool)
	Push(value interface{})
	Peek() (interface{}, bool)

	container.Container
}
