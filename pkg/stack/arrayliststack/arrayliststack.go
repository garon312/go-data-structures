package arrayliststack

import "go-data-structures/pkg/list/arraylist"

type Stack struct {
	list *arraylist.List
}

func New() *Stack {
	return &Stack{
		arraylist.New(),
	}
}

func (s *Stack) Pop() (interface{}, bool) {
	value, ok := s.list.Get(s.list.Size() - 1)
	s.list.Remove(s.list.Size() - 1)
	return value, ok
}

func (s *Stack) Push(value interface{}) {
	s.list.Add(value)
}

func (s *Stack) Peek() (interface{}, bool) {
	return s.list.Get(s.list.Size() - 1)
}

func (s *Stack) Empty() bool {
	return s.list.Size() == 0
}

func (s *Stack) Size() int {
	return s.list.Size()
}

func (s *Stack) Clear() {
	s.list.Clear()
}

func (s *Stack) Values() []interface{} {
	size := s.list.Size()
	elements := make([]interface{}, size, size)

	for i := 1; i <= size; i++ {
		elements[size-i], _ = s.list.Get(i - 1)
	}

	return elements
}
