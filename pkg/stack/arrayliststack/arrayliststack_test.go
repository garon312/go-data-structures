package arrayliststack

import (
	"fmt"
	"testing"
)

func TestStack_Push(t *testing.T) {
	input := []int{1, 2, 3}
	stack := New()

	for _, value := range input {
		stack.Push(value)
	}

	if val, ok := stack.Peek(); val != input[len(input)-1] || !ok {
		t.Errorf("Expected %v, got %v", input[len(input)-1], val)
	}

	if stack.Size() != len(input) {
		t.Errorf("Expected %v, got %v", stack.Size(), len(input))
	}
}

func TestStack_Pop(t *testing.T) {
	testCases := []struct {
		input        []interface{}
		expectedSize int
		expectedVal  interface{}
		expectedOk   bool
	}{
		{
			input:        []interface{}{1, 2, 3},
			expectedSize: 2,
			expectedVal:  3,
			expectedOk:   true,
		},
		{
			input:        []interface{}{1},
			expectedSize: 0,
			expectedVal:  1,
			expectedOk:   true,
		},
		{
			input:        []interface{}{},
			expectedSize: 0,
			expectedVal:  nil,
			expectedOk:   false,
		},
	}

	for _, testCase := range testCases {
		stack := New()

		for _, val := range testCase.input {
			stack.Push(val)
		}

		value, ok := stack.Pop()

		if ok != testCase.expectedOk {
			t.Errorf("Expected %v, got %v", testCase.expectedOk, ok)
		}

		if value != testCase.expectedVal {
			t.Errorf("Expected %v, got %v", testCase.expectedVal, value)
		}

		if stack.Size() != testCase.expectedSize {
			t.Errorf("Expected %v, got %v", testCase.expectedSize, stack.Size())
		}
	}
}

func TestStack_Values(t *testing.T) {
	input := []int{1, 2, 3}
	stack := New()

	for _, value := range input {
		stack.Push(value)
	}

	reversedInput := make([]int, len(input))

	for i := len(input) - 1; i >= 0; i-- {
		reversedInput[len(input)-i-1] = input[i]
	}

	if fmt.Sprint(reversedInput) != fmt.Sprint(stack.Values()) {
		t.Errorf("Expected %v, got %v", fmt.Sprint(reversedInput), fmt.Sprint(stack.Values()))
	}
}

func TestStack_Peek(t *testing.T) {
	input := []int{1, 2, 3}
	stack := New()

	for _, value := range input {
		stack.Push(value)
	}

	value, ok := stack.Peek()

	if !ok {
		t.Errorf("Expected %v, got %v", true, ok)
	}

	if value != input[len(input) - 1] {
		t.Errorf("Expected %v, got %v", input[len(input) - 1], value)
	}

	if stack.Size() != len(input) {
		t.Errorf("Expected %v, got %v", len(input), stack.Size())
	}
}
