package arraylist

const (
	growthFactor = float32(2.0)
	shrinkFactor = float32(0.25)
)

type List struct {
	elements []interface{}
	size     int
}

func New(values ...interface{}) *List {
	list := &List{}

	if len(values) > 0 {
		list.Add(values...)
	}

	return list
}

func (l *List) Get(index int) (interface{}, bool) {
	if !l.withinRange(index) {
		return nil, false
	}

	return l.elements[index], true
}

func (l *List) Set(index int, value interface{}) {
	if !l.withinRange(index) {
		return
	}

	l.elements[index] = value
}

func (l *List) Add(values ...interface{}) {
	l.expand(len(values))

	for _, value := range values {
		l.elements[l.size] = value
		l.size++
	}
}

func (l *List) Contains(value interface{}) bool {
	for _, listVal := range l.elements {
		if listVal == value {
			return true
		}
	}

	return false
}

func (l *List) Remove(index int) {
	if !l.withinRange(index) {
		return
	}

	l.elements[index] = nil
	copy(l.elements[index:], l.elements[index+1:l.size])
	l.size--
}

func (l *List) Empty() bool {
	return l.size == 0
}

func (l *List) Size() int {
	return l.size
}

func (l *List) Clear() {
	l.size = 0
	l.elements = []interface{}{}
}

func (l *List) Values() []interface{} {
	newElements := make([]interface{}, l.size, l.size)
	copy(newElements, l.elements[:l.size])
	return newElements
}

func (l *List) withinRange(index int) bool {
	return index >= 0 && index < l.size
}

func (l *List) resize(cap int) {
	newElements := make([]interface{}, cap, cap)
	copy(newElements, l.elements)
	l.elements = newElements
}

func (l *List) expand(n int) {
	currentCapacity := cap(l.elements)

	if l.size+n >= currentCapacity {
		newCapacity := int(growthFactor * float32(currentCapacity+n))
		l.resize(newCapacity)
	}
}

func (l *List) shrink() {
	if shrinkFactor == 0.0 {
		return
	}

	currentCapacity := cap(l.elements)
	if l.size <= int(float32(currentCapacity)*shrinkFactor) {
		l.resize(l.size)
	}
}
