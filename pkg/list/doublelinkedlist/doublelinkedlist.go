package doublelinkedlist

type List struct {
	first *element
	last  *element
	size  int
}

type element struct {
	value interface{}
	prev  *element
	next  *element
}

func New(values ...interface{}) *List {
	list := &List{}

	if len(values) > 0 {
		list.Add(values...)
	}

	return list
}

func (l *List) Get(index int) (interface{}, bool) {
	if !l.withinRange(index) {
		return nil, false
	}

	if l.size-index < index {
		element := l.last
		for e := l.size - 1; e != index; e, element = e-1, element.prev {
		}
		return element.value, true
	}

	element := l.first
	for e := 0; e != index; e, element = e+1, element.next {
	}

	return element.value, true
}

func (l *List) Set(index int, value interface{}) {
	if !l.withinRange(index) {
		return
	}

	var foundElement *element

	if l.size-index < index {
		foundElement = l.last
		for e := l.size - 1; e != index; e, foundElement = e-1, foundElement.prev {
		}
	} else {
		foundElement = l.first
		for e := 0; e != index; e, foundElement = e+1, foundElement.next {
		}
	}

	if foundElement != nil {
		foundElement.value = value
	}
}

func (l *List) Add(values ...interface{}) {
	for _, value := range values {
		newElement := &element{value: value, prev: l.last}

		if l.size == 0 {
			l.first = newElement
			l.last = newElement
		} else {
			l.last.next = newElement
			l.last = newElement
		}

		l.size++
	}
}

func (l *List) Contains(value interface{}) bool {
	if l.size == 0 {
		return false
	}

	for e := l.first; e != nil; e = e.next {
		if e.value == value {
			return true
		}
	}

	return false
}

func (l *List) Remove(index int) {
	if l.size == 1 {
		l.Clear()
		return
	}

	var element *element

	if l.size-index < index {
		element = l.last
		for e := l.size - 1; e != index; e, element = e-1, element.prev {
		}
	} else {
		element = l.first
		for e := 0; e != index; e, element = e+1, element.next {
		}
	}

	if element == l.first {
		l.first = element.next
	}

	if element == l.last {
		l.last = element.prev
	}

	if element.prev != nil {
		element.prev.next = element.next
	}

	if element.next != nil {
		element.next.prev = element.prev
	}

	element = nil

	l.size--
}

func (l *List) Empty() bool {
	return l.size == 0
}

func (l *List) Size() int {
	return l.size
}

func (l *List) Clear() {
	l.size = 0
	l.first = nil
	l.last = nil
}

func (l *List) Values() []interface{} {
	values := make([]interface{}, l.size, l.size)
	for e, element := 0, l.first; element != nil; e, element = e+1, element.next {
		values[e] = element.value
	}

	return values
}

func (l *List) withinRange(index int) bool {
	return index >= 0 && index < l.size
}
