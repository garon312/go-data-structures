package list

import "go-data-structures/pkg/container"

type List interface {
	Get(index int) (interface{}, bool)
	Set(index int, value interface{})
	Add(values ...interface{})
	Contains(value interface{}) bool
	Remove(index int)

	container.Container
}
