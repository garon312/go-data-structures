package linkedlist

import (
	"fmt"
	"testing"
)

func TestList_Get(t *testing.T) {
	list := New()
	input := []int{1, 2, 3}

	for _, value := range input {
		list.Add(value)
	}

	if len(list.Values()) != len(input) {
		t.Errorf("Expected %v, got %v", len(input), len(list.Values()))
	}

	if actualValue, ok := list.Get(0); actualValue != 1 || !ok {
		t.Errorf("Expected %v, got %v", 1, actualValue)
	}

	if actualValue, ok := list.Get(1); actualValue != 2 || !ok {
		t.Errorf("Expected %v, got %v", 3, actualValue)
	}

	if actualValue, ok := list.Get(2); actualValue != 3 || !ok {
		t.Errorf("Expected %v, got %v", 4, actualValue)
	}
}

func TestList_Set(t *testing.T) {
	input := []int{1, 2, 3}
	list := New()

	for _, value := range input {
		list.Add(value)
	}

	if len(list.Values()) != len(input) {
		t.Errorf("Expected %v, got %v", len(input), len(list.Values()))
	}

	for idx, value := range input {
		list.Set(idx, value+1)
	}

	for idx := range input {
		if actualValue, ok := list.Get(idx); actualValue != input[idx]+1 || !ok {
			t.Errorf("Expected %v, got %v", input[idx]+1, actualValue)
		}
	}
}

func TestList_Add(t *testing.T) {
	input := []int{1, 2, 3}
	list := New()

	for _, value := range input {
		list.Add(value)
	}

	if len(list.Values()) != len(input) {
		t.Errorf("Expected %v, got %v", len(input), len(list.Values()))
	}

	if empty := list.Empty(); empty != false {
		t.Errorf("Expected %v, got %v", false, empty)
	}

	if size := list.Size(); size != len(input) {
		t.Errorf("Expected %v, got %v", false, size)
	}

	if value, ok := list.Get(2); value != 3 || !ok {
		t.Errorf("Expected %v, got %v", value, 3)
	}
}

func TestList_Values(t *testing.T) {
	input := []int{1, 2, 3}
	list := New()

	for _, value := range input {
		list.Add(value)
	}

	if len(list.Values()) != len(input) {
		t.Errorf("Expected %v, got %v", len(input), len(list.Values()))
	}

	if fmt.Sprint(input) != fmt.Sprint(list.Values()) {
		t.Errorf("Expected %v, got %v", fmt.Sprint(input), fmt.Sprint(list.Values()))
	}
}

func TestList_Contains(t *testing.T) {
	input := []int{1, 2, 3}
	list := New()

	for _, value := range input {
		list.Add(value)
	}

	if len(list.Values()) != len(input) {
		t.Errorf("Expected %v, got %v", len(input), len(list.Values()))
	}

	if ok := list.Contains(1); !ok {
		t.Errorf("Expected %v, got %v", true, ok)
	}
}
