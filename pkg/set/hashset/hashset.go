package hashset

type Set struct {
	elements map[interface{}]bool
}

func New(elements ...interface{}) *Set {
	set := &Set{elements: make(map[interface{}]bool)}

	if len(elements) > 0 {
		set.Add(elements...)
	}

	return set
}

func (s Set) Add(elements ...interface{}) {
	for _, element := range elements {
		s.elements[element] = true
	}
}

func (s Set) Remove(elements ...interface{}) {
	for _, element := range elements {
		delete(s.elements, element)
	}
}

func (s Set) Contains(element interface{}) bool {
	if _, ok := s.elements[element]; ok {
		return true
	}

	return false
}

func (s Set) Empty() bool {
	return s.Size() == 0
}

func (s Set) Size() int {
	return len(s.elements)
}

func (s Set) Clear() {
	s.elements = make(map[interface{}]bool)
}

func (s Set) Values() []interface{} {
	values := make([]interface{}, s.Size())
	count := 0
	for element := range s.elements {
		values[count] = element
		count++
	}

	return values
}
