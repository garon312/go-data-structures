package hashset

import (
	"testing"
)

func TestSet_Add(t *testing.T) {
	set := New()
	set.Add(1)
	set.Add(2, 1)

	if !set.Contains(1) {
		t.Errorf("Expected %v, got %v", true, !set.Contains(1))
	}

	if !set.Contains(2) {
		t.Errorf("Expected %v, got %v", true, !set.Contains(2))
	}

	if set.Size() != 2 {
		t.Errorf("Expected %v, got %v", 2, set.Size())
	}
}

func TestSet_Remove(t *testing.T) {
	input := []int{1, 1, 2, 3}
	set := New()

	for _, value := range input {
		set.Add(value)
	}

	if set.Size() != 3 {
		t.Errorf("Expected %v, got %v", 2, set.Size())
	}

	set.Remove(1, 2)

	if set.Size() != 1 {
		t.Errorf("Expected %v, got %v", 1, set.Size())
	}

	if set.Contains(1) {
		t.Errorf("Expected %v, got %v", false, set.Contains(1))
	}

	if set.Contains(2) {
		t.Errorf("Expected %v, got %v", false, set.Contains(2))
	}

	if !set.Contains(3) {
		t.Errorf("Expected %v, got %v", true, !set.Contains(3))
	}
}

func TestList_Contains(t *testing.T) {
	input := []int{1, 2, 3}
	list := New()

	for _, value := range input {
		list.Add(value)
	}

	if ok := list.Contains(1); !ok {
		t.Errorf("Expected %v, got %v", true, ok)
	}
}
