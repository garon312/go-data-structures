package set

type Set interface {
	Add(elements ...interface{})
	Remove(elements ...interface{})
	Contains(element interface{}) bool
}
